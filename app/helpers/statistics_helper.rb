module StatisticsHelper
  def books_created_at_chart
    chartColor = "#FFFFFF"
    gradientFill = "#1e3d60"
    line_chart statistics_created_books_path, 
    dataset: {
      label: "Books",
      borderColor: chartColor,
      pointBorderColor: chartColor,
      pointBackgroundColor: "#1e3d60",
      pointHoverBackgroundColor: "#1e3d60",
      pointHoverBorderColor: chartColor,
      pointBorderWidth: 1,
      pointHoverRadius: 7,
      pointHoverBorderWidth: 2,
      pointRadius: 5,
      fill: true,
      backgroundColor: "rgba(255,255,255,.2)",
      borderWidth: 2,
    },
    library: {
      layout: {
        padding: {
          left: 20,
          right: 20,
          top: 0,
          bottom: 35
        }
      },
      title: {
        display: true,
        text: 'Created Books by Date',
        fontColor: "rgba(255,255,255,.8)",
      },
      maintainAspectRatio: false,
      tooltips: {
        backgroundColor: '#fff',
        titleFontColor: '#333',
        bodyFontColor: '#666',
        bodySpacing: 4,
        xPadding: 12,
        mode: "nearest",
        intersect: 0,
        position: "nearest"
      },
      legend: {
        position: "top",
        fillStyle: "#FFF",
      },
      scales: {
        yAxes: [{
          ticks: {
            fontColor: "rgba(255,255,255,.8)",
            fontStyle: "bold",
            beginAtZero: true,
            maxTicksLimit: 5,
            padding: 10
          },
          gridLines: {
            drawTicks: true,
            drawBorder: false,
            display: true,
            color: "rgba(255,255,255,0.1)",
            zeroLineColor: "transparent"
          }

        }],
        xAxes: [{
          gridLines: {
            zeroLineColor: "transparent",
            display: false,

          },
          ticks: {
            padding: 10,
            fontColor: "rgba(255,255,255,.8)",
            fontStyle: "bold"
          }
        }]
      }
    }
  end

  def books_by_authors_chart
    chartColor = "#FFFFFF"
    gradientFill = "#1e3d60"
    bar_chart statistics_books_by_authors_path
  end

  def books_by_publishers_chart
    chartColor = "#FFFFFF"
    gradientFill = "#1e3d60"
    bar_chart statistics_books_by_publishers_path
  end
end
