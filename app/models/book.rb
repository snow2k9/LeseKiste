class Book < ApplicationRecord
  belongs_to :author, inverse_of: :books
  belongs_to :publisher, inverse_of: :books

  accepts_nested_attributes_for :author, reject_if: :all_blank?
  accepts_nested_attributes_for :publisher, reject_if: :all_blank?
end
