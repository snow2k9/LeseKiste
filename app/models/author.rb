class Author < ApplicationRecord
  has_many :books, inverse_of: :author
  has_many :publishers, through: :books, inverse_of: :authors
end
