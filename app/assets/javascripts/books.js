$(document).on('turbolinks:load', function() {
  authors.initialize();

  $('.author.typeahead').typeahead(
    {
      hint            : true,
      highlight       : true,
      minLength       : 2
    },
    {
      name: 'searchable-authors',
      displayKey: 'name',
      source: authors.ttAdapter()
  });

  publishers.initialize();

  $('.publisher.typeahead').typeahead(
    {
      hint            : true,
      highlight       : true,
      minLength       : 2
    },
    {
      name: 'searchable-publishers',
      displayKey: 'name',
      source: publishers.ttAdapter()
  });
});

var authors = new Bloodhound({
  datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
  queryTokenizer: Bloodhound.tokenizers.whitespace,
  prefetch: {
    url: '/search/authors'
  },
  remote: {
    url: '/search/authors/%QUERY',
    wildcard: '%QUERY',
    filter: function(response) { return response.data.results; }
  },
  limit: 20
});

var publishers = new Bloodhound({
  datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
  queryTokenizer: Bloodhound.tokenizers.whitespace,
  prefetch: {
    url: '/search/publishers'
  },
  remote: {
    url: '/search/publishers/%QUERY',
    wildcard: '%QUERY',
    filter: function(response) { return response.data.results; }
  },
  limit: 20
});