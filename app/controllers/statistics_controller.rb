class StatisticsController < ApplicationController
  def created_books
    render json: Book.all.group('created_at::date').count
  end

  def books_by_authors
    render json: Book.joins(:author).all.group(:full_name).count
  end

  def books_by_publishers
    render json: Book.joins(:publisher).all.group(:name).count
  end
end
