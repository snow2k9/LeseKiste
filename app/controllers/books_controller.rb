class BooksController < ApplicationController
  before_action :set_book, only: [:show,:edit, :update, :destroy]

  # GET /books
  # GET /books.json
  def index
    @books = Book.eager_load(:author, :publisher).all
  end

  # GET /books/1
  # GET /books/1.json
  def show
  end

  # GET /books/new
  def new
    @book = Book.new
    @book.build_author
    @book.build_publisher
  end

  # GET /books/1/edit
  def edit
  end

  # POST /books
  # POST /books.json
  def create
    author = Author.where(book_params[:author_attributes]).first_or_create(full_name: book_params[:author_attributes][:full_name], display_name: book_params[:author_attributes][:full_name])
    publisher = Publisher.where(book_params[:publisher_attributes]).first_or_create
    @book = Book.where(title: book_params[:title], isbn: book_params[:isbn], author: author, publisher: publisher).first_or_initialize

    respond_to do |format|
      if @book.save
        format.html { redirect_to books_path, notice: 'Book was successfully created.' }
        format.json { render :show, status: :created, location: @book }
      else
        format.html { render :new }
        format.json { render json: @book.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /books/1
  # PATCH/PUT /books/1.json
  def update
    author = Author.where(full_name: book_params[:author_attributes][:full_name]).first_or_create(full_name: book_params[:author_attributes][:full_name], display_name: book_params[:author_attributes][:full_name])
    publisher = Publisher.where(name: book_params[:publisher_attributes][:name]).first_or_create
    
    respond_to do |format|
      if @book.update(title: book_params[:title], isbn: book_params[:isbn], author: author, publisher: publisher)
        format.html { redirect_to books_path, notice: 'Book was successfully updated.' }
        format.json { render :show, status: :ok, location: @book }
      else
        format.html { render :edit }
        format.json { render json: @book.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /books/1
  # DELETE /books/1.json
  def destroy
    @book.destroy
    respond_to do |format|
      format.html { redirect_to books_url, notice: 'Book was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_book
      @book = Book.find(params[:id])
    end

    def book_params
      params.require(:book).permit(:title, :isbn, author_attributes: [:id, :full_name], publisher_attributes: [:id, :name])
    end
  end
