Rails.application.routes.draw do
  scope :search do
    get 'authors(/:query)', as: 'author_search', action: 'author_search', controller: 'authors'
    get 'publishers(/:query)', as: 'publisher_search', action: 'publisher_search', controller: 'publishers'
  end
  resources :books
  resources :publishers
  resources :authors 
  


  namespace :statistics do
    get 'books/created', as: 'created_books', action: 'created_books'
    get 'books/authors', as: 'books_by_authors', action: 'books_by_authors'
    get 'books/publishers', as: 'books_by_publishers', action: 'books_by_publishers'
  end
  root 'dashboard#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
